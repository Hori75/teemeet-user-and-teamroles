# Teemeet TeamRole

[![pipeline status](https://gitlab.com/Hori75/teemeet-user-and-teamroles/badges/master/pipeline.svg)](https://gitlab.com/Hori75/teemeet-user-and-teamroles/-/commits) 
[![coverage report](https://gitlab.com/Hori75/teemeet-user-and-teamroles/badges/master/coverage.svg)](https://gitlab.com/Hori75/teemeet-user-and-teamroles/-/commits)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?branch=master&project=Hori75_teemeet-user-and-teamroles&metric=alert_status)](https://sonarcloud.io/dashboard?id=Hori75_teemeet-user-and-teamroles&branch=master)


- dev branch

[![pipeline status](https://gitlab.com/Hori75/teemeet-user-and-teamroles/badges/dev/pipeline.svg)](https://gitlab.com/Hori75/teemeet-user-and-teamroles/-/commits) 
[![coverage report](https://gitlab.com/Hori75/teemeet-user-and-teamroles/badges/dev/coverage.svg)](https://gitlab.com/Hori75/teemeet-user-and-teamroles/-/commits)

This is a microservice that handles group roles and permissions for actions in TeemeeT group features.
