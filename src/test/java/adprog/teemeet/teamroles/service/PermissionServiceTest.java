package adprog.teemeet.teamroles.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import adprog.teemeet.teamroles.core.MemberAndRole;
import adprog.teemeet.teamroles.model.AssignedTeamRole;
import adprog.teemeet.teamroles.model.TeamRole;
import adprog.teemeet.teamroles.model.TeamRoleDTO;
import java.util.Arrays;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PermissionServiceTest {
    @Mock
    private TeamRolesService teamRolesService;

    @Mock
    private AssignedTeamRoleService assignedTeamRoleService;

    @InjectMocks
    private PermissionServiceImpl permissionService;

    private TeamRoleDTO teamRoleDTO;
    private TeamRole teamRole1;
    private TeamRole teamRole2;
    private AssignedTeamRole assignedTeamRole1;
    private AssignedTeamRole assignedTeamRole2;
    private long roleId;
    private long kodeGroup;
    private long memberId;

    /**
     * Sets up related Objects for testing.
     */
    @BeforeEach
    public void setUp() {
        teamRoleDTO = new TeamRoleDTO();
        teamRoleDTO.setName("dummyRole");
        teamRoleDTO.setManageRoles(false);
        teamRoleDTO.setManageTasks(false);
        teamRoleDTO.setManageEvents(false);

        roleId = 0;
        kodeGroup = 0;
        memberId = 0;

        teamRole1 = new TeamRole();
        teamRole1.setTeamRole(roleId, kodeGroup, teamRoleDTO);

        assignedTeamRole1 = new AssignedTeamRole();
        assignedTeamRole1.setRoleId(roleId);
        assignedTeamRole1.setMemberId(memberId);

        roleId = 1;
        teamRoleDTO.setManageRoles(true);

        teamRole2 = new TeamRole();
        teamRole2.setTeamRole(roleId, kodeGroup, teamRoleDTO);

        assignedTeamRole2 = new AssignedTeamRole();
        assignedTeamRole2.setRoleId(roleId);
        assignedTeamRole2.setMemberId(memberId);
    }

    @Test
    public void testGetRolePermissions() throws Exception {
        Iterable<AssignedTeamRole> list = Arrays.asList(assignedTeamRole1, assignedTeamRole2);
        when(assignedTeamRoleService.getAllByMemberId(memberId)).thenReturn(list);
        when(teamRolesService.getTeamRoleById(0)).thenReturn(Optional.ofNullable(teamRole1));
        when(teamRolesService.getTeamRoleById(1)).thenReturn(Optional.ofNullable(teamRole2));

        MemberAndRole permissions = permissionService.getRolePermissions(memberId);

        assertTrue(permissions.hasPermissionToManageRoles());
        assertFalse(permissions.hasPermissionToManageTasks());
        assertFalse(permissions.hasPermissionToManageEvents());
    }

    @Test
    public void testGetRolePermissionsSkipNull() throws Exception {
        Iterable<AssignedTeamRole> list = Arrays.asList(assignedTeamRole1, assignedTeamRole2);
        when(assignedTeamRoleService.getAllByMemberId(memberId)).thenReturn(list);
        when(teamRolesService.getTeamRoleById(0)).thenReturn(Optional.ofNullable(null));
        when(teamRolesService.getTeamRoleById(1)).thenReturn(Optional.ofNullable(teamRole2));

        MemberAndRole permissions = permissionService.getRolePermissions(memberId);

        assertTrue(permissions.hasPermissionToManageRoles());
        assertFalse(permissions.hasPermissionToManageTasks());
        assertFalse(permissions.hasPermissionToManageEvents());
    }

    @Test
    public void testGetRolePermissionsReturnAdminRole() throws Exception {
        teamRole1.setAdminRole(true);
        Iterable<AssignedTeamRole> list = Arrays.asList(assignedTeamRole1, assignedTeamRole2);
        when(assignedTeamRoleService.getAllByMemberId(memberId)).thenReturn(list);
        when(teamRolesService.getTeamRoleById(0)).thenReturn(Optional.ofNullable(teamRole1));

        MemberAndRole permissions = permissionService.getRolePermissions(memberId);

        assertTrue(permissions.hasPermissionToManageRoles());
        assertTrue(permissions.hasPermissionToManageTasks());
        assertTrue(permissions.hasPermissionToManageEvents());
    }
}