package adprog.teemeet.teamroles.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import adprog.teemeet.teamroles.model.AssignedTeamRole;
import adprog.teemeet.teamroles.model.TeamRole;
import adprog.teemeet.teamroles.model.TeamRoleDTO;
import adprog.teemeet.teamroles.repository.AssignedTeamRoleRepository;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class AssignedTeamRoleServiceTest {

    @Mock
    private AssignedTeamRoleRepository assignedTeamRoleRepository;

    @Mock
    private TeamRolesServiceImpl teamRolesService;

    @InjectMocks
    private AssignedTeamRoleServiceImpl assignedTeamRoleService;

    private TeamRoleDTO teamRoleDTO;
    private TeamRole teamRole1;
    private TeamRole teamRole2;
    private AssignedTeamRole assignedTeamRole1;
    private AssignedTeamRole assignedTeamRole2;
    private long roleId;
    private long kodeGroup;
    private long memberId;

    /**
     * Sets up related Objects for testing.
     */
    @BeforeEach
    public void setUp() {
        teamRoleDTO = new TeamRoleDTO();
        teamRoleDTO.setName("dummyRole");
        teamRoleDTO.setManageRoles(false);
        teamRoleDTO.setManageTasks(false);
        teamRoleDTO.setManageEvents(false);

        roleId = 0;
        kodeGroup = 0;
        memberId = 0;

        teamRole1 = new TeamRole();
        teamRole1.setTeamRole(roleId, kodeGroup, teamRoleDTO);

        assignedTeamRole1 = new AssignedTeamRole();
        assignedTeamRole1.setRoleId(roleId);
        assignedTeamRole1.setMemberId(memberId);

        roleId = 1;
        teamRoleDTO.setManageRoles(true);

        teamRole2 = new TeamRole();
        teamRole2.setTeamRole(roleId, kodeGroup, teamRoleDTO);

        assignedTeamRole2 = new AssignedTeamRole();
        assignedTeamRole2.setRoleId(roleId);
        assignedTeamRole2.setMemberId(memberId);
    }

    @Test
    public void testGetAllByRoleId() throws Exception {
        List<AssignedTeamRole> atrlist = Arrays.asList(assignedTeamRole2);
        when(assignedTeamRoleRepository.findAllByRoleId(roleId)).thenReturn(atrlist);

        Iterable<AssignedTeamRole> resultList = assignedTeamRoleService.getAllByRoleId(roleId);
        assertNotNull(resultList);
    }
    
    @Test
    public void testGetAllByMemberId() throws Exception {
        List<AssignedTeamRole> atrlist = Arrays.asList(assignedTeamRole1, assignedTeamRole2);
        when(assignedTeamRoleRepository.findAllByMemberId(memberId)).thenReturn(atrlist);

        Iterable<AssignedTeamRole> resultList = assignedTeamRoleService.getAllByMemberId(memberId);
        assertNotNull(resultList);
    }

    @Test
    public void testGetByRoleIdAndMemberId() throws Exception {
        when(assignedTeamRoleRepository.findByRoleIdAndMemberId(roleId, memberId))
            .thenReturn(Optional.ofNullable(assignedTeamRole2));

        assertEquals(
            assignedTeamRole2,
            assignedTeamRoleService.getByRoleIdAndMemberId(roleId, memberId).get()
        );
    }

    @Test
    public void testAssignTeamRole() throws Exception {
        when(teamRolesService.getTeamRoleById(roleId)).thenReturn(Optional.ofNullable(teamRole2));
        when(assignedTeamRoleRepository.save(any(AssignedTeamRole.class)))
            .thenReturn(assignedTeamRole2);

        AssignedTeamRole atr = 
            assignedTeamRoleService.assignTeamRole(kodeGroup, memberId, roleId);

        assertEquals(assignedTeamRole2, atr);
    }

    @Test
    public void testAssignTeamRoleButDoesNotExist() throws Exception {

        assertThrows(
            Exception.class, 
            () -> assignedTeamRoleService.assignTeamRole(kodeGroup, memberId, roleId)
        );
    }

    @Test
    public void testAssignTeamRoleButDoesNotExistInGroup() throws Exception {
        when(teamRolesService.getTeamRoleById(roleId)).thenReturn(Optional.ofNullable(teamRole2));
        kodeGroup = 2;

        assertThrows(
            Exception.class, 
            () -> assignedTeamRoleService.assignTeamRole(kodeGroup, memberId, roleId)
        );
    }

    @Test
    public void testUnassignTeamRole() throws Exception {
        when(assignedTeamRoleRepository.findByRoleIdAndMemberId(roleId, memberId))
            .thenReturn(Optional.ofNullable(assignedTeamRole2));
        when(teamRolesService.getTeamRoleById(roleId))
            .thenReturn(Optional.ofNullable(teamRole2));

        assignedTeamRoleService.unassignTeamRole(memberId, roleId);
    }

    @Test
    public void testUnassignNonExistentTeamRole() throws Exception {
        when(assignedTeamRoleRepository.findByRoleIdAndMemberId(roleId, memberId))
            .thenReturn(Optional.ofNullable(assignedTeamRole2));

        assertThrows(
            Exception.class, 
            () -> assignedTeamRoleService.unassignTeamRole(memberId, roleId)
        );
    }

    @Test
    public void testUnassignTeamRoleButDoesNotExist() throws Exception {

        assertThrows(
            Exception.class, 
            () -> assignedTeamRoleService.unassignTeamRole(memberId, roleId)
        );
    }
}
