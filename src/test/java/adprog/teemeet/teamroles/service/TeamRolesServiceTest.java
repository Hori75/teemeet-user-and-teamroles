package adprog.teemeet.teamroles.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import adprog.teemeet.teamroles.model.AssignedTeamRole;
import adprog.teemeet.teamroles.model.TeamRole;
import adprog.teemeet.teamroles.model.TeamRoleDTO;
import adprog.teemeet.teamroles.repository.TeamRolesRepository;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TeamRolesServiceTest {
    @Mock
    private TeamRolesRepository teamRolesRepository;

    @Mock
    private AssignedTeamRoleService assignedTeamRoleService;

    @InjectMocks
    private TeamRolesServiceImpl teamRolesService;

    private AssignedTeamRole atr;
    private TeamRoleDTO teamRoleDTO;
    private TeamRole teamRole;
    private long memberId;
    private long roleId;
    private long kodeGroup;

    /**
     * Sets up TeamRole Object for testing.
     */
    @BeforeEach
    public void setUp() {
        teamRoleDTO = new TeamRoleDTO();
        teamRoleDTO.setName("dummyRole");
        teamRoleDTO.setManageRoles(false);
        teamRoleDTO.setManageTasks(false);
        teamRoleDTO.setManageEvents(false);
        teamRoleDTO.setAdmin(false);

        kodeGroup = 0;
        roleId = 0;
        memberId = 0;

        teamRole = new TeamRole();
        teamRole.setTeamRole(roleId, kodeGroup, teamRoleDTO);

        atr = new AssignedTeamRole();
        atr.setMemberId(memberId);
        atr.setRoleId(roleId);
    }

    @Test
    public void testServiceCreateTeamRole() throws Exception {
        when(teamRolesRepository.save(any(TeamRole.class))).thenReturn(teamRole);
        assertEquals(teamRole, teamRolesService.createTeamRole(kodeGroup, teamRoleDTO));
    }

    @Test
    public void testServiceGetListTeamRole() throws Exception {
        List<TeamRole> listTeamRole = Arrays.asList(teamRole);
        when(teamRolesRepository.findAll()).thenReturn(listTeamRole);
        Iterable<TeamRole> listTeamRoleResult = teamRolesService.getListTeamRole();
        assertIterableEquals(listTeamRole, listTeamRoleResult);
    }

    @Test
    public void testServiceGetListTeamRoleByGroup() throws Exception {
        List<TeamRole> listTeamRole = Arrays.asList(teamRole);
        when(teamRolesRepository.findAllByKodeGroup(0)).thenReturn(listTeamRole);
        Iterable<TeamRole> listTeamRoleResult = teamRolesService.getListTeamRoleByGroup(0);
        assertIterableEquals(listTeamRole, listTeamRoleResult);
    }

    @Test
    public void testServiceGetTeamRoleById() throws Exception {
        when(teamRolesRepository.findById(teamRole.getRoleId()))
            .thenReturn(Optional.ofNullable(teamRole));
        TeamRole resultTeamRole = teamRolesService.getTeamRoleById(teamRole.getRoleId()).get();
        assertEquals(teamRole, resultTeamRole);
    }

    @Test
    public void testServiceDeleteTeamRole() throws Exception {
        List<AssignedTeamRole> atrList = Arrays.asList(atr);
        when(assignedTeamRoleService.getAllByRoleId(roleId))
            .thenReturn(atrList);
        teamRolesService.deleteTeamRole(teamRole);
        when(teamRolesRepository.findById(teamRole.getRoleId()))
            .thenReturn(Optional.ofNullable(null));
        assertEquals(
            Optional.ofNullable(null), 
            teamRolesService.getTeamRoleById(teamRole.getRoleId())
        );
    }

    @Test
    public void testServiceDeleteTeamRoleThrowsException() throws Exception {
        List<AssignedTeamRole> atrList = Arrays.asList(atr);
        when(assignedTeamRoleService.getAllByRoleId(roleId))
            .thenReturn(atrList);
        doThrow(new Exception("something wrong happened"))
            .when(assignedTeamRoleService).unassignTeamRole(memberId, roleId);
        assertThrows(
            Exception.class, 
            () -> teamRolesService.deleteTeamRole(teamRole)
        );
    }

    @Test
    public void testServiceUpdateTeamRole() throws Exception {

        teamRoleDTO.setName("Dummy2");
        TeamRole otherTeamRole = new TeamRole();
        otherTeamRole.setTeamRole(teamRole.getRoleId(), teamRole.getKodeGroup(), teamRoleDTO);

        when(teamRolesRepository.save(any(TeamRole.class))).thenReturn(otherTeamRole);
        TeamRole resultTeamRole = 
            teamRolesService.updateTeamRole(teamRole.getRoleId(), teamRoleDTO, teamRole);

        assertEquals(teamRole, resultTeamRole);
        assertEquals(otherTeamRole.getName(), resultTeamRole.getName());
    }

    @Test
    public void testServiceUpdateTeamRoleAdmin() throws Exception {
        teamRole.setAdminRole(true);

        assertThrows(
            Exception.class, 
            () -> teamRolesService.updateTeamRole(teamRole.getRoleId(), teamRoleDTO, teamRole)
        );
    }
}

