package adprog.teemeet.teamroles.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import adprog.teemeet.teamroles.core.DefaultAdminRole;
import adprog.teemeet.teamroles.core.Member;
import adprog.teemeet.teamroles.core.MemberAndRole;
import adprog.teemeet.teamroles.service.PermissionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@WithMockUser(username = "spring")
@SpringBootTest
@AutoConfigureMockMvc
public class PermissionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PermissionService permissionService;

    private MemberAndRole permissions;

    @BeforeEach
    public void setUp() {
        permissions = new Member();
    }

    @Test
    public void testGetMemberPermission() throws Exception {
        when(permissionService.getRolePermissions(0)).thenReturn(permissions);

        mvc.perform(get("/roles/permission/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.roles").value(false))
                .andExpect(jsonPath("$.tasks").value(false))
                .andExpect(jsonPath("$.events").value(false));
    }

    @Test
    public void testGetAdminPermission() throws Exception {
        permissions = new DefaultAdminRole(permissions);
        when(permissionService.getRolePermissions(0)).thenReturn(permissions);

        mvc.perform(get("/roles/permission/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.roles").value(true))
                .andExpect(jsonPath("$.tasks").value(true))
                .andExpect(jsonPath("$.events").value(true));
    }

    
}
