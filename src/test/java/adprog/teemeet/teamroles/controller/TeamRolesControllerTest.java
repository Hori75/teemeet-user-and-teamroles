package adprog.teemeet.teamroles.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import adprog.teemeet.teamroles.model.TeamRole;
import adprog.teemeet.teamroles.model.TeamRoleDTO;
import adprog.teemeet.teamroles.service.TeamRolesServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@WithMockUser(username = "spring")
@SpringBootTest
@AutoConfigureMockMvc
public class TeamRolesControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TeamRolesServiceImpl teamRolesService;

    private TeamRoleDTO teamRoleDTO;
    private TeamRole teamRole;
    private long roleId;
    private long kodeGroup;

    /**
     * Sets up TeamRole object for testing.
     */
    @BeforeEach
    public void setUp() {

        teamRoleDTO = new TeamRoleDTO();
        teamRoleDTO.setName("dummyRole");
        teamRoleDTO.setManageRoles(false);
        teamRoleDTO.setManageTasks(false);
        teamRoleDTO.setManageEvents(false);
        teamRoleDTO.setAdmin(false);

        roleId = 0;
        kodeGroup = 0;

        teamRole = new TeamRole();
        teamRole.setTeamRole(roleId, kodeGroup, teamRoleDTO);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerPostTeamRole() throws Exception {

        when(teamRolesService.createTeamRole(
            any(Long.class), 
            any(TeamRoleDTO.class)
        ))
            .thenReturn(teamRole);

        mvc.perform(post("/roles/create/0")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(teamRoleDTO)))
                .andExpect(jsonPath("$.roleId").value(0))
                .andExpect(jsonPath("$.kodeGroup").value(0))
                .andExpect(jsonPath("$.name").value("dummyRole"))
                .andExpect(jsonPath("$.manageRoles").value(false))
                .andExpect(jsonPath("$.manageTasks").value(false))
                .andExpect(jsonPath("$.manageEvents").value(false));
    }

    @Test
    public void testControllerGetListTeamRole() throws Exception {

        Iterable<TeamRole> listTeamRole = Arrays.asList(teamRole);
        when(teamRolesService.getListTeamRole()).thenReturn(listTeamRole);

        mvc.perform(get("/roles").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].roleId").value(0))
                .andExpect(jsonPath("$[0].kodeGroup").value(0))
                .andExpect(jsonPath("$[0].name").value("dummyRole"))
                .andExpect(jsonPath("$[0].manageRoles").value(false))
                .andExpect(jsonPath("$[0].manageTasks").value(false))
                .andExpect(jsonPath("$[0].manageEvents").value(false));
    }

    @Test
    public void testControllerGetListTeamRoleByGroup() throws Exception {

        Iterable<TeamRole> listTeamRole = Arrays.asList(teamRole);
        when(teamRolesService.getListTeamRoleByGroup(0)).thenReturn(listTeamRole);

        mvc.perform(get("/roles/group/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].roleId").value(0))
                .andExpect(jsonPath("$[0].kodeGroup").value(0))
                .andExpect(jsonPath("$[0].name").value("dummyRole"))
                .andExpect(jsonPath("$[0].manageRoles").value(false))
                .andExpect(jsonPath("$[0].manageTasks").value(false))
                .andExpect(jsonPath("$[0].manageEvents").value(false));
    }

    @Test
    public void testControllerGetTeamRoleById() throws Exception {

        when(teamRolesService.getTeamRoleById(0))
            .thenReturn(Optional.ofNullable(teamRole));
        mvc.perform(get("/roles/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.roleId").value(0))
                .andExpect(jsonPath("$.kodeGroup").value(0))
                .andExpect(jsonPath("$.name").value("dummyRole"))
                .andExpect(jsonPath("$.manageRoles").value(false))
                .andExpect(jsonPath("$.manageTasks").value(false))
                .andExpect(jsonPath("$.manageEvents").value(false));
    }

    @Test
    public void testControllerGetNonExistTeamRole() throws Exception {
        mvc.perform(get("/roles/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerUpdateTeamRole() throws Exception {

        teamRoleDTO.setName("Dummy2");
        teamRole.setTeamRole(kodeGroup, teamRoleDTO);

        when(teamRolesService.getTeamRoleById(teamRole.getRoleId()))
            .thenReturn(Optional.ofNullable(teamRole));
        when(teamRolesService.updateTeamRole(
            any(Long.class), any(TeamRoleDTO.class), any(TeamRole.class))
        )
            .thenReturn(teamRole);

        mvc.perform(put("/roles/0").contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(teamRoleDTO)))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.roleId").value(0))
                .andExpect(jsonPath("$.kodeGroup").value(0))
                .andExpect(jsonPath("$.name").value("Dummy2"))
                .andExpect(jsonPath("$.manageRoles").value(false))
                .andExpect(jsonPath("$.manageTasks").value(false))
                .andExpect(jsonPath("$.manageEvents").value(false));
    }

    @Test
    public void testControllerUpdateTeamRoleBadRequest() throws Exception {

        teamRoleDTO.setName("Dummy2");
        teamRole.setTeamRole(kodeGroup, teamRoleDTO);
        teamRole.setAdminRole(true);

        when(teamRolesService.getTeamRoleById(teamRole.getRoleId()))
            .thenReturn(Optional.ofNullable(teamRole));
        when(teamRolesService.updateTeamRole(
            any(Long.class), any(TeamRoleDTO.class), any(TeamRole.class))
        )
            .thenThrow(new Exception("something wrong happened"));

        mvc.perform(put("/roles/0").contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(teamRoleDTO)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testControllerUpdateNonExistTeamRole() throws Exception {

        teamRoleDTO.setName("Dummy2");
        teamRole.setTeamRole(kodeGroup, teamRoleDTO);

        mvc.perform(put("/roles/1").contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(teamRoleDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerDeleteTeamRole() throws Exception {

        when(teamRolesService.getTeamRoleById(teamRole.getRoleId()))
            .thenReturn(Optional.ofNullable(teamRole));

        mvc.perform(delete("/roles/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerDeleteTeamRoleBadRequest() throws Exception {

        when(teamRolesService.getTeamRoleById(teamRole.getRoleId()))
            .thenReturn(Optional.ofNullable(teamRole));

        doThrow(new Exception("something wrong happened"))
            .when(teamRolesService).deleteTeamRole(teamRole);

        mvc.perform(delete("/roles/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testControllerDeleteNonExistTeamRole() throws Exception {

        mvc.perform(delete("/roles/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}

