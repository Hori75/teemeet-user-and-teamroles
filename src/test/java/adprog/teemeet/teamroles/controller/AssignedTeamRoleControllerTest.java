package adprog.teemeet.teamroles.controller;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import adprog.teemeet.teamroles.model.AssignedTeamRole;
import adprog.teemeet.teamroles.service.AssignedTeamRoleService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@WithMockUser(username = "spring")
@SpringBootTest
@AutoConfigureMockMvc
public class AssignedTeamRoleControllerTest {
    
    @Autowired
    private MockMvc mvc;

    @MockBean
    private AssignedTeamRoleService assignedTeamRoleService;

    private AssignedTeamRole assignedTeamRole;
    private long kodeGroup;
    private long roleId;
    private long memberId;

    /**
     * sets Objects before each tests.
     */
    @BeforeEach
    public void setUp() {
        roleId = 0;
        memberId = 0;
        assignedTeamRole = new AssignedTeamRole();
        assignedTeamRole.setRoleId(roleId);
        assignedTeamRole.setMemberId(memberId);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testGetAssignedTeamRoleByRoleId() throws Exception {
        Iterable<AssignedTeamRole> list = Arrays.asList(assignedTeamRole);
        when(assignedTeamRoleService.getAllByRoleId(assignedTeamRole.getRoleId()))
            .thenReturn(list);

        mvc.perform(get("/roles/assign/role/0")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[0].roleId").value(roleId))
                .andExpect(jsonPath("$[0].memberId").value(memberId));
    }

    @Test
    public void testGetAssignedTeamRoleByMemberId() throws Exception {
        Iterable<AssignedTeamRole> list = Arrays.asList(assignedTeamRole);
        when(assignedTeamRoleService.getAllByMemberId(assignedTeamRole.getMemberId()))
            .thenReturn(list);

        mvc.perform(get("/roles/assign/member/0")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[0].roleId").value(roleId))
                .andExpect(jsonPath("$[0].memberId").value(memberId));
    }

    @Test
    public void testAssignTeamRole() throws Exception {
        kodeGroup = 0;
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("roleId", roleId);
        map.put("memberId", memberId);

        when(assignedTeamRoleService.getByRoleIdAndMemberId(roleId, memberId))
            .thenReturn(Optional.ofNullable(null));
        when(assignedTeamRoleService.assignTeamRole(kodeGroup, memberId, roleId))
            .thenReturn(assignedTeamRole);

        mvc.perform(post("/roles/assign/0")
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(map)))
            .andExpect(jsonPath("$.roleId").value(roleId))
            .andExpect(jsonPath("$.memberId").value(memberId));
    }

    @Test
    public void testAssignTeamRoleAlreadyAssigned() throws Exception {
        kodeGroup = 0;
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("roleId", roleId);
        map.put("memberId", memberId);

        when(assignedTeamRoleService.getByRoleIdAndMemberId(roleId, memberId))
            .thenReturn(Optional.ofNullable(assignedTeamRole));

        mvc.perform(post("/roles/assign/0")
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(map)))
            .andExpect(jsonPath("$.roleId").value(roleId))
            .andExpect(jsonPath("$.memberId").value(memberId));
    }

    @Test
    public void testAssignTeamRoleaBadRequest() throws Exception {
        kodeGroup = 1;
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("roleId", roleId);
        map.put("memberId", memberId);

        when(assignedTeamRoleService.assignTeamRole(kodeGroup, memberId, roleId))
            .thenThrow(new Exception("TeamRole doesn't exist!"));

        mvc.perform(post("/roles/assign/1")
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(map)))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testUnassignTeamRole() throws Exception {
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("roleId", roleId);
        map.put("memberId", memberId);

        mvc.perform(delete("/roles/assign/")
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(map)))
            .andExpect(status().isNoContent());
    }

    @Test
    public void testUnassignTeamRoleBadRequest() throws Exception {
        roleId = 1;
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("roleId", roleId);
        map.put("memberId", memberId);

        doThrow(new Exception("No such assigned TeamRole!"))
            .when(assignedTeamRoleService).unassignTeamRole(memberId, roleId);

        mvc.perform(delete("/roles/assign/")
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(map)))
            .andExpect(status().isBadRequest());
    }
}
