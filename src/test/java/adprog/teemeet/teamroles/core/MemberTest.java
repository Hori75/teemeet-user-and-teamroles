package adprog.teemeet.teamroles.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MemberTest {
    private Class<?> memberClass;
    private Member member;

    @BeforeEach
    public void setUp() throws Exception {
        memberClass = Class.forName("adprog.teemeet.teamroles.core.Member");
        member = new Member();
    }

    @Test
    public void testMemberIsConcreteClass() {
        assertFalse(Modifier
                .isAbstract(memberClass.getModifiers()));
    }

    @Test
    public void testMemberIsAMemberAndRole() {
        assertTrue(member instanceof MemberAndRole);
    }

    @Test
    public void testMemberOverrideHasPermissionToManageRolesMethod() throws Exception {
        Method hasPermissionToManageRoles = 
            memberClass.getDeclaredMethod("hasPermissionToManageRoles");

        assertEquals("boolean",
            hasPermissionToManageRoles.getGenericReturnType().getTypeName());
        assertEquals(0,
            hasPermissionToManageRoles.getParameterCount());
        assertTrue(Modifier.isPublic(hasPermissionToManageRoles.getModifiers()));
    }

    @Test
    public void testMemberOverrideHasPermissionToManageTasksMethod() throws Exception {
        Method hasPermissionToManageTasks = 
            memberClass.getDeclaredMethod("hasPermissionToManageTasks");

        assertEquals("boolean",
            hasPermissionToManageTasks.getGenericReturnType().getTypeName());
        assertEquals(0,
            hasPermissionToManageTasks.getParameterCount());
        assertTrue(Modifier.isPublic(hasPermissionToManageTasks.getModifiers()));
    }

    @Test
    public void testMemberOverrideHasPermissionToManageEventsMethod() throws Exception {
        Method hasPermissionToManageEvents = 
            memberClass.getDeclaredMethod("hasPermissionToManageEvents");

        assertEquals("boolean",
            hasPermissionToManageEvents.getGenericReturnType().getTypeName());
        assertEquals(0,
            hasPermissionToManageEvents.getParameterCount());
        assertTrue(Modifier.isPublic(hasPermissionToManageEvents.getModifiers()));
    }

    @Test
    public void testMemberHasPermissionToManageRolesReturnsFalse() throws Exception {
        assertFalse(member.hasPermissionToManageRoles());
    }

    @Test
    public void testMemberHasPermissionToManageTasksReturnsFalse() throws Exception {
        assertFalse(member.hasPermissionToManageTasks());
    }

    @Test
    public void testMemberHasPermissionToManageEventsReturnsFalse() throws Exception {
        assertFalse(member.hasPermissionToManageEvents());
    }
}
