package adprog.teemeet.teamroles.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MemberAndRoleTest {
    private Class<?> memberAndRoleClass;

    @BeforeEach
    public void setup() throws Exception {
        memberAndRoleClass = Class.forName("adprog.teemeet.teamroles.core.MemberAndRole");
    }

    @Test
    public void testMemberAndRoleIsAPublicInterface() {
        int classModifiers = memberAndRoleClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMemberAndRoleHasPermissionToManageRolesAbstractMethod() throws Exception {
        Method hasPermissionToManageRoles = 
            memberAndRoleClass.getDeclaredMethod("hasPermissionToManageRoles");
        int methodModifiers = hasPermissionToManageRoles.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testMemberAndRoleHasPermissionToManageTasksAbstractMethod() throws Exception {
        Method hasPermissionToManageTasks = 
            memberAndRoleClass.getDeclaredMethod("hasPermissionToManageTasks");
        int methodModifiers = hasPermissionToManageTasks.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testMemberAndRoleHasPermissionToManageEventsAbstractMethod() throws Exception {
        Method hasPermissionToManageEventsMethod = 
            memberAndRoleClass.getDeclaredMethod("hasPermissionToManageEvents");
        int methodModifiers = hasPermissionToManageEventsMethod.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

}
