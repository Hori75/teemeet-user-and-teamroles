package adprog.teemeet.teamroles.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RoleTest {
    private Class<?> roleClass;
    private Role role;

    @BeforeEach
    public void setUp() throws Exception {
        roleClass = Class.forName("adprog.teemeet.teamroles.core.Role");
        role = new Role(false, false, false, new Member());
    }

    @Test
    public void testRoleIsConcreteClass() {
        assertFalse(Modifier
                .isAbstract(roleClass.getModifiers()));
    }

    @Test
    public void testRoleIsARoleAndMember() {
        assertTrue(role instanceof MemberAndRole);
    }

    @Test
    public void testRoleOverrideHasPermissionToManageRolesMethod() throws Exception {
        Method hasPermissionToManageRoles = 
            roleClass.getDeclaredMethod("hasPermissionToManageRoles");

        assertEquals("boolean",
            hasPermissionToManageRoles.getGenericReturnType().getTypeName());
        assertEquals(0,
            hasPermissionToManageRoles.getParameterCount());
        assertTrue(Modifier.isPublic(hasPermissionToManageRoles.getModifiers()));
    }

    @Test
    public void testRoleOverrideHasPermissionToManageTasksMethod() throws Exception {
        Method hasPermissionToManageTasks = 
            roleClass.getDeclaredMethod("hasPermissionToManageTasks");

        assertEquals("boolean",
            hasPermissionToManageTasks.getGenericReturnType().getTypeName());
        assertEquals(0,
            hasPermissionToManageTasks.getParameterCount());
        assertTrue(Modifier.isPublic(hasPermissionToManageTasks.getModifiers()));
    }

    @Test
    public void testRoleOverrideHasPermissionToManageEventsMethod() throws Exception {
        Method hasPermissionToManageEvents = 
            roleClass.getDeclaredMethod("hasPermissionToManageEvents");

        assertEquals("boolean",
            hasPermissionToManageEvents.getGenericReturnType().getTypeName());
        assertEquals(0,
            hasPermissionToManageEvents.getParameterCount());
        assertTrue(Modifier.isPublic(hasPermissionToManageEvents.getModifiers()));
    }

    @Test
    public void testRoleHasPermissionToManageRolesReturnsTrueIfOneOfTheRolesIsTrue() 
        throws Exception {
        MemberAndRole otherRole = new Role(true, false, false, new Member());
        role = new Role(false, false, false, otherRole);
        assertTrue(role.hasPermissionToManageRoles());
    }

    @Test
    public void testRoleHasPermissionToManageTasksReturnsTrueIfOneOfTheRolesIsTrue() 
        throws Exception {
        MemberAndRole otherRole = new Role(false, true, false, new Member());
        role = new Role(false, false, false, otherRole);
        assertTrue(role.hasPermissionToManageTasks());
    }

    @Test
    public void testRoleHasPermissionToManageEventsReturnsTrueIfOneOfTheRolesIsTrue() 
        throws Exception {
        MemberAndRole otherRole = new Role(false, false, true, new Member());
        role = new Role(false, false, false, otherRole);
        assertTrue(role.hasPermissionToManageEvents());
    }

}
