package adprog.teemeet.teamroles.core;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DefaultAdminRoleTest {
    private Class<?> defaultAdminRoleClass;
    private DefaultAdminRole defaultAdminRole;

    @BeforeEach
    public void setUp() throws Exception {
        defaultAdminRoleClass = Class.forName("adprog.teemeet.teamroles.core.DefaultAdminRole");
        defaultAdminRole = new DefaultAdminRole(new Member());
    }

    @Test
    public void testDefaultAdminRoleIsConcreteClass() {
        assertFalse(Modifier
                .isAbstract(defaultAdminRoleClass.getModifiers()));
    }

    @Test
    public void testDefaultAdminRoleIsAMemberAndRole() {
        assertTrue(defaultAdminRole instanceof MemberAndRole);
    }

    @Test
    public void testDefaultAdminRoleHasPermissionToManageRolesReturnsTrue() throws Exception {
        assertTrue(defaultAdminRole.hasPermissionToManageRoles());
    }

    @Test
    public void testDefaultAdminRoleHasPermissionToManageTasksReturnsTrue() throws Exception {
        assertTrue(defaultAdminRole.hasPermissionToManageTasks());
    }

    @Test
    public void testDefaultAdminRoleHasPermissionToManageEventsReturnsTrue() throws Exception {
        assertTrue(defaultAdminRole.hasPermissionToManageEvents());
    }
}
