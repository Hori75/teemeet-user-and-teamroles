package adprog.teemeet.teamroles.repository;

import adprog.teemeet.teamroles.model.TeamRole;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRolesRepository extends JpaRepository<TeamRole, Long> {

    List<TeamRole> findAllByKodeGroup(long kodeGroup);
}