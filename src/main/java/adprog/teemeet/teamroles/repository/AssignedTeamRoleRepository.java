package adprog.teemeet.teamroles.repository;

import adprog.teemeet.teamroles.model.AssignedTeamRole;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssignedTeamRoleRepository extends JpaRepository<AssignedTeamRole, Long> {
    
    Iterable<AssignedTeamRole> findAllByRoleId(long roleId);

    Iterable<AssignedTeamRole> findAllByMemberId(long memberId);

    Optional<AssignedTeamRole> findByRoleIdAndMemberId(long roleId, long memberId);
}
