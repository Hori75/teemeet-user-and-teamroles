package adprog.teemeet.teamroles.controller;

import adprog.teemeet.teamroles.model.TeamRole;
import adprog.teemeet.teamroles.model.TeamRoleDTO;
import adprog.teemeet.teamroles.service.TeamRolesService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/roles")
@CrossOrigin
public class TeamRolesController {
    @Autowired
    private TeamRolesService teamRolesService;

    /**
     * Handles Post Request to create teamRole.
     * @param kodeGroup group
     * @param teamRoleDTO teamRole DTO
     * @return created teamRole or bad request
     */
    @PostMapping(path = "/create/{kodeGroup}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> postTeamRole(
        @PathVariable(value = "kodeGroup") long kodeGroup,
        @RequestBody TeamRoleDTO teamRoleDTO
    ) {
        TeamRole teamRole = teamRolesService.createTeamRole(kodeGroup, teamRoleDTO);
        return ResponseEntity.ok(teamRole);
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TeamRole>> getListTeamRole() {
        return ResponseEntity.ok(teamRolesService.getListTeamRole());
    }

    /**
     * Handles GET request to get list of TeamRoles based on kodeGroup.
     * @param kodeGroup kode of the requested group
     * @return list of TeamRoles with same kodeGroup
     */
    @GetMapping(path = "/group/{kodeGroup}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TeamRole>> getListTeamRoleByGroup(
        @PathVariable(value = "kodeGroup") long kodeGroup
    ) {
        return ResponseEntity.ok(teamRolesService.getListTeamRoleByGroup(kodeGroup));
    }

    /**
     * Handles GET request to get a TeamRoles with a matching Id.
     * @param roleId id of a requested TeamRole
     * @return a TeamRole with matching Id
     */
    @GetMapping(path = "/{roleId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> getTeamRole(
        @PathVariable(value = "roleId") long roleId
    ) {
        Optional<TeamRole> teamRole = teamRolesService.getTeamRoleById(roleId);
        if (teamRole.isEmpty()) {
            return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(teamRole.get());
    }

    /**
     * Handles PUT request to modify a TeamRole with a matching Id.
     * @param roleId id of a requested TeamRole
     * @param teamRoleDTO modified TeamRole
     * @return a modified TeamRole
     */
    @PutMapping(path = "/{roleId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> updateTeamRole(
        @PathVariable(value = "roleId") long roleId, 
        @RequestBody TeamRoleDTO teamRoleDTO
    ) {
        Optional<TeamRole> teamRoleTarget = teamRolesService.getTeamRoleById(roleId);
        if (teamRoleTarget.isEmpty()) {
            return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
        }
        TeamRole teamRole;
        try {
            teamRole = teamRolesService.updateTeamRole(roleId, teamRoleDTO, teamRoleTarget.get());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(teamRole);
    }

    /**
     * Handles DELETE request to delete a TeamRole with matching Id.
     * @param roleId id of a requested TeamRole
     * @return No Content or Bad Request
     */
    @DeleteMapping(path = "/{roleId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> deleteTeamRole(
        @PathVariable(value = "roleId") long roleId
    ) {
        Optional<TeamRole> teamRole = teamRolesService.getTeamRoleById(roleId);
        if (teamRole.isEmpty()) {
            return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
        }
        try {
            teamRolesService.deleteTeamRole(teamRole.get());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);
    }
    
}

