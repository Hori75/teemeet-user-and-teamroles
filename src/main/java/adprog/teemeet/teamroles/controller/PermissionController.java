package adprog.teemeet.teamroles.controller;

import adprog.teemeet.teamroles.core.MemberAndRole;
import adprog.teemeet.teamroles.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/roles/permission")
@CrossOrigin
public class PermissionController {

    @Autowired
    private PermissionService permissionService;
    
    @GetMapping(path = "/{memberId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> getPermissions(@PathVariable("memberId") long memberId) {
        MemberAndRole permissions = permissionService.getRolePermissions(memberId);
        return ResponseEntity.ok(permissions.getPermissionDict());
    }

}
