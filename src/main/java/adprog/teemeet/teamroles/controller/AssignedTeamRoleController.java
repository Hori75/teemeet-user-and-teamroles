package adprog.teemeet.teamroles.controller;

import adprog.teemeet.teamroles.model.AssignedTeamRole;
import adprog.teemeet.teamroles.service.AssignedTeamRoleService;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/roles/assign")
@CrossOrigin
public class AssignedTeamRoleController {

    @Autowired
    private AssignedTeamRoleService assignedTeamRoleService;

    @GetMapping(path = "/role/{roleId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> getAssignedTeamRoleByRoleId(
        @PathVariable(value = "roleId") long roleId
    ) {
        Iterable<AssignedTeamRole> list = assignedTeamRoleService.getAllByRoleId(roleId);
        return ResponseEntity.ok(list);
    }

    @GetMapping(path = "/member/{memberId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> getAssignedTeamRoleByMemberId(
        @PathVariable(value = "memberId") long memberId
    ) {
        Iterable<AssignedTeamRole> list = assignedTeamRoleService.getAllByMemberId(memberId);
        return ResponseEntity.ok(list);
    }
    
    /**
     * Assigns a member to a teamRole.
     * @param kodeGroup the group id
     * @param args roleId, memberId, assignerId
     * @return assigned teamRole
     */
    @PostMapping(path = "/{kodeGroup}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> assignTeamRole(
        @PathVariable(value = "kodeGroup") long kodeGroup,
        @RequestBody Map<String, Long> args
    ) {
        AssignedTeamRole assignedTeamRole;
        long roleId = args.get("roleId");
        long memberId = args.get("memberId");
        Optional<AssignedTeamRole> atr = 
            assignedTeamRoleService.getByRoleIdAndMemberId(roleId, memberId);
        if (atr.isEmpty()) {
            try {
                assignedTeamRole = 
                    assignedTeamRoleService.assignTeamRole(kodeGroup, memberId, roleId);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        } else {
            assignedTeamRole = atr.get();
        }
        return ResponseEntity.ok(assignedTeamRole);
    }

    /**
     * Unassigns a member from a teamRole.
     * @param args roleId, memberId, assignerId
     * @return no content
     */
    @DeleteMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> unassignTeamRole(
        @RequestBody Map<String, Long> args
    ) {
        long roleId = args.get("roleId");
        long memberId = args.get("memberId");
        try { 
            assignedTeamRoleService.unassignTeamRole(memberId, roleId);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);
    }
}
