package adprog.teemeet.teamroles.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "assignedteamrole")
@Table(name = "assignedteamrole")
@Data
public class AssignedTeamRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "atrid", updatable = false, nullable = false)
    private long atrId;

    @Column(name = "roleid", updatable = false, nullable = false)
    private long roleId;

    @Column(name = "memberid", updatable = false, nullable = false)
    private long memberId;

}