package adprog.teemeet.teamroles.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Data
public class TeamRoleDTO {

    private String name;
    private Boolean manageRoles;
    private Boolean manageTasks;
    private Boolean manageEvents;
    private Boolean admin;
    
}
