package adprog.teemeet.teamroles.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "teamrole")
@Table(name = "teamrole")
@Data
public class TeamRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "roleid", updatable = false, nullable = false)
    private Long roleId;

    @Column(name = "kodegroup", updatable = false, nullable = false)
    private Long kodeGroup;

    @Column(name = "name")
    private String name;

    @Builder.Default
    @Column(name = "manageroles")
    private Boolean manageRoles = false;

    @Builder.Default
    @Column(name = "managetasks")
    private Boolean manageTasks = false;

    @Builder.Default
    @Column(name = "manageEvents")
    private Boolean manageEvents = false;

    @Builder.Default
    @Column(name = "adminrole")
    private Boolean adminRole = false;

    /**
     * Setter using roleId, kodeGroup, and TeamRoleDTO.
     * @param roleId roleId
     * @param kodeGroup kodeGroup
     * @param teamRoleDTO teamRoleDTO
     */
    public void setTeamRole(Long roleId, Long kodeGroup, TeamRoleDTO teamRoleDTO) {
        this.setRoleId(roleId);
        this.setKodeGroup(kodeGroup);
        this.setName(teamRoleDTO.getName());
        this.setManageRoles(teamRoleDTO.getManageRoles());
        this.setManageTasks(teamRoleDTO.getManageTasks());
        this.setManageEvents(teamRoleDTO.getManageEvents());
    }

    /**
     * Setter using kodeGroup, and TeamRoleDTO.
     * @param kodeGroup kodeGroup
     * @param teamRoleDTO teamRoleDTO
     */
    public void setTeamRole(Long kodeGroup, TeamRoleDTO teamRoleDTO) {
        this.setKodeGroup(kodeGroup);
        this.setName(teamRoleDTO.getName());
        this.setManageRoles(teamRoleDTO.getManageRoles());
        this.setManageTasks(teamRoleDTO.getManageTasks());
        this.setManageEvents(teamRoleDTO.getManageEvents());
    }

}
