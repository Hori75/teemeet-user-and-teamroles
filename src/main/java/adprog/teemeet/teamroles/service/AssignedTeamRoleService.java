package adprog.teemeet.teamroles.service;

import adprog.teemeet.teamroles.model.AssignedTeamRole;
import java.util.Optional;

public interface AssignedTeamRoleService {
    
    public Iterable<AssignedTeamRole> getAllByRoleId(long roleId);

    public Iterable<AssignedTeamRole> getAllByMemberId(long memberId);

    public Optional<AssignedTeamRole> getByRoleIdAndMemberId(long roleId, long memberId);

    public AssignedTeamRole assignTeamRole(
        long kodeGroup, 
        long memberId, 
        long roleId
    ) throws Exception;

    public void unassignTeamRole(
        long memberId, 
        long roleId
    ) throws Exception;
}
