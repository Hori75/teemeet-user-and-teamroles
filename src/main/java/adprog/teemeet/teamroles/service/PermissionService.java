package adprog.teemeet.teamroles.service;

import adprog.teemeet.teamroles.core.MemberAndRole;

public interface PermissionService {
    
    public MemberAndRole getRolePermissions(long memberId);
    
}
