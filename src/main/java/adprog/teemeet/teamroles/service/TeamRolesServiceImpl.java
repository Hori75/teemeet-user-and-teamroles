package adprog.teemeet.teamroles.service;

import adprog.teemeet.teamroles.model.AssignedTeamRole;
import adprog.teemeet.teamroles.model.TeamRole;
import adprog.teemeet.teamroles.model.TeamRoleDTO;
import adprog.teemeet.teamroles.repository.TeamRolesRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamRolesServiceImpl implements TeamRolesService {
    
    @Autowired
    private TeamRolesRepository teamRolesRepository;

    @Autowired
    private AssignedTeamRoleService assignedTeamRoleService;

    public Iterable<TeamRole> getListTeamRole() {
        return teamRolesRepository.findAll();
    }

    public Iterable<TeamRole> getListTeamRoleByGroup(long kodeGroup) {
        return teamRolesRepository.findAllByKodeGroup(kodeGroup);
    }

    public Optional<TeamRole> getTeamRoleById(long id) {
        return teamRolesRepository.findById(id);
    }

    /**
     * creates teamRole.
     * @param kodeGroup kodeGroup to be stored
     * @param teamRoleDTO new TeamRole data
     * @return saved teamRole
     */
    public TeamRole createTeamRole(long kodeGroup, TeamRoleDTO teamRoleDTO) {
        TeamRole teamRole = new TeamRole();
        if (teamRoleDTO.getAdmin()) {
            teamRole.setAdminRole(teamRoleDTO.getAdmin());
        }
        teamRole.setTeamRole(kodeGroup, teamRoleDTO);
        return teamRolesRepository.save(teamRole);
    }

    /**
     * updates teamRole while keeping the previous kodeGroup.
     * @param roleId target Id for update
     * @param teamRoleDTO modified teamRole
     * @param teamRole TeamRole target
     * @return saved teamRole
     */
    public TeamRole updateTeamRole(long roleId, TeamRoleDTO teamRoleDTO, TeamRole teamRole) 
        throws Exception {
        if (teamRole.getAdminRole()) {
            throw new Exception("Could not update default admin role!");
        }
        teamRole.setTeamRole(teamRole.getRoleId(), teamRole.getKodeGroup(), teamRoleDTO);
        return teamRolesRepository.save(teamRole);
    }

    /**
     * deletes teamRole and the assignment in that role.
     * @param teamRole to be deleted
     * @throws Exception could not delete default admin role
     */
    public void deleteTeamRole(TeamRole teamRole) throws Exception {
        Iterable<AssignedTeamRole> atrList = 
            assignedTeamRoleService.getAllByRoleId(teamRole.getRoleId());
        for (AssignedTeamRole atr : atrList) {
            try {
                assignedTeamRoleService.unassignTeamRole(atr.getMemberId(), teamRole.getRoleId());
            } catch (Exception e) {
                throw e;
            }
        }
        teamRolesRepository.delete(teamRole);
    }
}
