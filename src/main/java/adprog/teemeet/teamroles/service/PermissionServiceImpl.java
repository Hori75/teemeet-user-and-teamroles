package adprog.teemeet.teamroles.service;

import adprog.teemeet.teamroles.core.DefaultAdminRole;
import adprog.teemeet.teamroles.core.Member;
import adprog.teemeet.teamroles.core.MemberAndRole;
import adprog.teemeet.teamroles.core.Role;
import adprog.teemeet.teamroles.model.AssignedTeamRole;
import adprog.teemeet.teamroles.model.TeamRole;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl implements PermissionService {
    
    @Autowired
    private AssignedTeamRoleService assignedTeamRoleService;

    @Autowired
    private TeamRolesService teamRolesService;

    @Override
    public MemberAndRole getRolePermissions(long memberId) {
        MemberAndRole rolePermission = new Member();
        Iterable<AssignedTeamRole> list = assignedTeamRoleService.getAllByMemberId(memberId);

        for (AssignedTeamRole atr : list) {
            Optional<TeamRole> optTeamRole = teamRolesService.getTeamRoleById(atr.getRoleId());
            if (optTeamRole.isEmpty()) {
                continue;
            }
            TeamRole teamRole = optTeamRole.get();
            if (teamRole.getAdminRole()) {
                return new DefaultAdminRole(rolePermission);
            } else {
                rolePermission = new Role(
                    teamRole.getManageRoles(), 
                    teamRole.getManageTasks(), 
                    teamRole.getManageEvents(), 
                    rolePermission
                );
            }
        }
        return rolePermission;
    }
}
