package adprog.teemeet.teamroles.service;

import adprog.teemeet.teamroles.model.AssignedTeamRole;
import adprog.teemeet.teamroles.model.TeamRole;
import adprog.teemeet.teamroles.repository.AssignedTeamRoleRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssignedTeamRoleServiceImpl implements AssignedTeamRoleService {

    @Autowired
    private AssignedTeamRoleRepository assignedTeamRoleRepository;

    @Autowired
    private TeamRolesService teamRolesService;

    @Override
    public Iterable<AssignedTeamRole> getAllByRoleId(long roleId) {
        return assignedTeamRoleRepository.findAllByRoleId(roleId);
    }

    @Override
    public Iterable<AssignedTeamRole> getAllByMemberId(long memberId) {
        return assignedTeamRoleRepository.findAllByMemberId(memberId);
    }

    @Override
    public Optional<AssignedTeamRole> getByRoleIdAndMemberId(long roleId, long memberId) {
        return assignedTeamRoleRepository.findByRoleIdAndMemberId(roleId, memberId);
    }
    
    @Override
    public AssignedTeamRole assignTeamRole(
        long kodeGroup, 
        long memberId, 
        long roleId
    ) throws Exception {
        Optional<TeamRole> teamRole =  teamRolesService.getTeamRoleById(roleId);
        if (teamRole.isEmpty()) {
            throw new Exception("TeamRole doesn't exist!");
        } else if (teamRole.get().getKodeGroup() != kodeGroup) {
            throw new Exception("TeamRole doesn't exist in this group!");
        }
        AssignedTeamRole assignedTeamRole = new AssignedTeamRole();
        assignedTeamRole.setRoleId(roleId);
        assignedTeamRole.setMemberId(memberId);
        return assignedTeamRoleRepository.save(assignedTeamRole);
    }

    @Override
    public void unassignTeamRole(
        long memberId, 
        long roleId
    ) throws Exception {
        Optional<AssignedTeamRole> assignedTeamRole = getByRoleIdAndMemberId(roleId, memberId);
        if (assignedTeamRole.isEmpty()) {
            throw new Exception("No such assigned TeamRole!");
        }
        Optional<TeamRole> teamRole = teamRolesService.getTeamRoleById(roleId);
        if (teamRole.isEmpty()) {
            throw new Exception("No such TeamRole!");
        } 
        assignedTeamRoleRepository.delete(assignedTeamRole.get());
    }
}
