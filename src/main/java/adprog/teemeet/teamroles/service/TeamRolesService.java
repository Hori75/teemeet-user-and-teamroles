package adprog.teemeet.teamroles.service;

import adprog.teemeet.teamroles.model.TeamRole;
import adprog.teemeet.teamroles.model.TeamRoleDTO;
import java.util.Optional;

public interface TeamRolesService {

    public Iterable<TeamRole> getListTeamRole();

    public Iterable<TeamRole> getListTeamRoleByGroup(long kodeGroup);

    public Optional<TeamRole> getTeamRoleById(long id);

    public TeamRole createTeamRole(long kodeGroup, TeamRoleDTO teamRoleDTO);

    public TeamRole updateTeamRole(long roleId, TeamRoleDTO teamRoleDTO, TeamRole teamRole)
        throws Exception;
    
    public void deleteTeamRole(TeamRole teamRole) throws Exception;
}
