package adprog.teemeet.teamroles.core;

public class DefaultAdminRole extends Role {
    
    public DefaultAdminRole(MemberAndRole memberOrRole) {
        super(true, true, true, memberOrRole);
    }
}
