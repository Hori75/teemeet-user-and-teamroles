package adprog.teemeet.teamroles.core;

import java.util.HashMap;
import java.util.Map;

public class Role implements MemberAndRole {

    private MemberAndRole memberOrRole;

    private Boolean manageRoles;

    private Boolean manageTasks;

    private Boolean manageEvents;

    /**
     * Creates new role.
     * @param manageRoles manage roles permission
     * @param manageTasks manage tasks permission
     * @param manageEvents manage events permision
     * @param memberOrRole not null
     */
    public Role(
        boolean manageRoles, 
        boolean manageTasks, 
        boolean manageEvents, 
        MemberAndRole memberOrRole
    ) {
        this.manageRoles = manageRoles;
        this.manageTasks = manageTasks;
        this.manageEvents = manageEvents;
        this.memberOrRole = memberOrRole;
    }

    /**
     * Checks this role and wrapped roles if the roles have permission to manage roles.
     * @return manageRoles for this role and wrapped roles
     */
    @Override
    public boolean hasPermissionToManageRoles() {
        if (manageRoles) {
            return true;
        } else {
            return memberOrRole.hasPermissionToManageRoles();
        }
    }

    /**
     * Checks this role and wrapped roles if the roles have permission to manage tasks.
     * @return manageTasks for this role and wrapped roles
     */
    @Override
    public boolean hasPermissionToManageTasks() {
        if (manageTasks) {
            return true;
        } else {
            return memberOrRole.hasPermissionToManageTasks();
        }
    }

    /**
     * Checks this role and wrapped roles if the roles have permission to manageEvents.
     * @return manageEvents for this role and wrapped roles
     */
    @Override
    public boolean hasPermissionToManageEvents() {
        if (manageEvents) {
            return true;
        } else {
            return memberOrRole.hasPermissionToManageEvents();
        }
    }

    @Override
    public Map<String, Boolean> getPermissionDict() {
        Map<String, Boolean> dict = new HashMap<String, Boolean>();
        dict.put("roles", hasPermissionToManageRoles());
        dict.put("tasks", hasPermissionToManageTasks());
        dict.put("events", hasPermissionToManageEvents());
        return dict;
    }
}
