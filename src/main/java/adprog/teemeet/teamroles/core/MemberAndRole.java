package adprog.teemeet.teamroles.core;

import java.util.Map;

public interface MemberAndRole {
    
    public boolean hasPermissionToManageRoles();

    public boolean hasPermissionToManageTasks();
    
    public boolean hasPermissionToManageEvents();

    public Map<String, Boolean> getPermissionDict();
}
