package adprog.teemeet.teamroles.core;

import java.util.HashMap;
import java.util.Map;

public class Member implements MemberAndRole {

    @Override
    public boolean hasPermissionToManageRoles() {
        return false;
    }

    @Override
    public boolean hasPermissionToManageTasks() {
        return false;
    }
    
    @Override
    public boolean hasPermissionToManageEvents() {
        return false;
    }

    @Override
    public Map<String, Boolean> getPermissionDict() {
        Map<String, Boolean> dict = new HashMap<String, Boolean>();
        dict.put("roles", hasPermissionToManageRoles());
        dict.put("tasks", hasPermissionToManageTasks());
        dict.put("events", hasPermissionToManageEvents());
        return dict;
    }
}
